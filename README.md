EgelaPy Script
------------
Python 2 script to download files from the University of the Basque Country Moodle (aka Egela)

## Requisites
To run the script, this libraries have to be installed:

- [mechanize](https://github.com/python-mechanize/mechanize) ```pip2 install --user mechanize```
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/) ```pip2 install --user beautifulsoup4```

## How to use it:

- Running in terminal ```python2 menu.py```.

## Usage
Once the script is executed, you have to log in into your Egela account. That's it.

![image](doc/EgelaPyUsage.png)
